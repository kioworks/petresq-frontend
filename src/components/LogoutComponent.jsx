import React, {Component} from 'react';

class LogoutComponent extends Component{
    render(){
        return (
            <React.Fragment>
                <h1>Logged out</h1>
                <div className="container">
                    Thank you
                </div>
            </React.Fragment>

        )
    }
}

export default LogoutComponent;