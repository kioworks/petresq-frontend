import React, { Component } from 'react';
import AuthenticationService from '../AuthenticationService';
import classes from './LoginComponent.module.css';


class LoginComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            hasLoginFail: false,
            showSuccessMessage: false

        }

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);

    }

    handleChange = (e) => {

        console.log(e.target.value);

        this.setState({
            [e.target.name]
                : e.target.value
        })
    }


    loginClicked() {

        if (this.state.username === 'shui' && this.state.password === 'dummy') {
            AuthenticationService.registerSuccessfulLogin(this.state.username,this.state.password);
            this.props.history.push(`/welcome/${this.state.username}`)
            this.setState({ showSuccessMessage: true })
            this.setState({ hasLoginFail: false })
        }

        else {
            console.log('failed')
            this.setState({ showSuccessMessage: false })
            this.setState({ hasLoginFail: true })
        }

    }




    render() {
        return (
            <div>
                <h1>Login</h1>
                <div className="container">
                {this.state.hasLoginFail && <div className="alert alert-warning">Invalid Credentials</div>}
                

                <div className="container">
                    User Name: <input type="text" name="username" value={this.state.username} onChange={this.handleChange} />
                    Password: <input type="password" name="password" value={this.state.password} onChange={this.handleChange} />
                    <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
                </div>
                </div>
            </div>
        )
    }
}

export default LoginComponent