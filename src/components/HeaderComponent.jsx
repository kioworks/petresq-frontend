import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AuthenticationService from '../AuthenticationService.js'
import { withRouter } from 'react-router'
import Logo from '../components/layout/Logo/Logo';
import classes from './HeaderComponent.module.css';

class HeaderComponent extends Component{
    render(){
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        console.log(isUserLoggedIn);
        return (
            <header>
                <nav className={classes.Toolbar}>
                
                    <div><a href="www.petresq.com"><Logo/> </a></div>
                    <ul className="navbar-nav">
                        {isUserLoggedIn && <li ><Link className="nav-link" to="/welcome">Home</Link></li>}
                        {isUserLoggedIn && <li ><Link className="nav-link" to="/posts">Posts</Link></li>}
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                    <li ><Link className="nav-link" to="/login">Login</Link></li>
                    {isUserLoggedIn && <li ><Link className="nav-link"to="/logout" onClick={AuthenticationService.logout}>Log out</Link></li>}
                    </ul>
                    

                </nav>
            </header>
        
        )
    }
}

export default withRouter(HeaderComponent);