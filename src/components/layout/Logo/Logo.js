import React from 'react';
import petLogo from '../../../assets/images/pets.png';
import classes from './Logo.module.css';


const logo = (props) => (

 <div className={classes.Logo}>
     <img src={petLogo} alt="Pets" />
 </div>

);

export default logo;