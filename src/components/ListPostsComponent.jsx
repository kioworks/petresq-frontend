import React, { Component } from 'react';
import PostDataService from './posts/PostDataService.js';
import AuthenticationService from '../AuthenticationService.js'

class ListPostsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: 
            [
            
            ]



        }
    }

    componentDidMount(){
        let username=AuthenticationService.getLoggedInUserName();

        PostDataService.retrieveAllPosts(username)
            .then(
                response => {
                    console.log(response);
                    this.setState({ posts : response.data })
    }
            )
    }

    render() {
        return (
        <div>
            <h1>List posts</h1>
            <div className="container">
            <tabel className="table">
                <thead>
                    <tr>
                        <th>info</th>
                        <th>Posted on</th>
                        <th>Status</th>

                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.posts.map(
                           post=>     
                           <tr key={post.id}>
                           <td>{post.info}</td>
                           <td>{post.submitDate.toString()}</td>
                           <td>{post.foundHome.toString()}</td>
                           

                            </tr>
                        )
                
                    }
                </tbody>
            </tabel>
            </div>
        </div>)
    }
}

export default ListPostsComponent;