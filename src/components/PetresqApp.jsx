import React, { Component } from 'react'
import { BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'
import '../../src/bootstrap.css'
import AuthenticationService from '../AuthenticationService.js'
import AuthenticatedRoute from'../AuthenticatedRoute.jsx'
import LoginComponent from './LoginComponent.jsx'
import ListPostsComponent from './ListPostsComponent.jsx'
import HeaderComponent from './HeaderComponent.jsx'
import FooterComponent from './FooterComponent.jsx'
import WelcomeComponent from './WelcomeComponent.jsx'
import LogoutComponent from './LogoutComponent.jsx'
import ErrorComponent from './ErrorComponent.jsx'
import classes from './PetresqApp.module.css';




class PetresqApp extends Component {
    render() {
        return (

            <div className="PetresqApp">
                <Router>
                    <React.Fragment>
                        <HeaderComponent/>
                        <main className={classes.Content}>
                        <Switch>
                            <Route path="/" exact component={LoginComponent} />
                            <Route path="/login" component={LoginComponent} />
                            <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent} />
                            <AuthenticatedRoute path="/posts" component={ListPostsComponent} />
                            <AuthenticatedRoute path="/logout" component={LogoutComponent} />
                            <Route component={ErrorComponent} />
                        </Switch>
                        </main>
                        <FooterComponent/>
                    </React.Fragment>

                </Router>

            </div>

        )
    }
}
























export default PetresqApp;