import React, {Component} from 'react';
import { Link } from 'react-router-dom'


class WelcomeComponent extends Component {
    render() {
        return(
            <React.Fragment>
                <h1>Welcome</h1>
                <div className="container">Welcome {this.props.match.params.name}. You can manage your posts <Link to="/posts">here</Link>.</div>
                

            </React.Fragment>



        ) 
    }
}

export default WelcomeComponent;